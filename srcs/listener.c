#include "ft_malcolm.h"

extern void *g_ptr;

int		check_recv_packet(void *buf, t_params *params)
{
	unsigned long	broadcast;
	t_arphdr		*tmp_arp;
	t_ethernethdr	*tmp_eth;

	tmp_eth = (t_ethernethdr *)buf;
	tmp_arp = (t_arphdr *)((char *)buf + sizeof(t_ethernethdr));
	ft_memset(&broadcast, 0xff, sizeof(unsigned long));
	if (params->opt & VERBOSE)
	{
		debug_my_ethernet(*tmp_eth);
		debug_my_arp(*tmp_arp);
	}
	if (ft_memcmp(tmp_eth->src_mac, &broadcast, 6))
		return KO;
	if (tmp_eth->protocol != htons(0x0806))
		return KO;
	if (tmp_arp->opcode != htons(0x01)) // request
		return KO;
	if (tmp_arp->protocol_type != htons(0x0800)) // IPV4
		return KO;
	if (ft_memcmp(params->src_mac, tmp_arp->src_mac, 6))
		return KO;
	ft_memset(&broadcast, 0x00, sizeof(unsigned long));
	if (ft_memcmp(&broadcast, tmp_arp->dst_mac, 6))
		return KO;
	if (params->src_ip != tmp_arp->src_ip || params->dst_ip != tmp_arp->dst_ip)
		return KO;
	return OK;
}

uint8_t		receive_ARP_request(int fd, t_params *params)
{
	int					ret;
	char				buf[512];
	struct sockaddr_in	from;
	socklen_t			len;

	ret = -1;
	len = sizeof(from);
	ret = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&from, &len);
	if (ret == -1)
		return KO;
	ret = check_recv_packet(buf, params);
	if (ret == OK)
		printf("An ARP request has been broadcast.\n");
	return ret;
}

uint8_t		listen_ARP_request_from_target(int fd, t_params *params)
{

	printf("listen_ARP_request_from_target\n");
	printf("waiting for packet, please wait...\n");
	for (int i = 0; g_ptr; i++)
	{
		// listen to ARP broadcast from src
		if (receive_ARP_request(fd, params) == OK)
			break;
	}
	if (!g_ptr)
		return KO;
	return OK;
}
