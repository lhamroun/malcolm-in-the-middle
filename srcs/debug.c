#include "ft_malcolm.h"

void    debug_network_address(uint32_t ip)
{
	printf("%u.%u.%u.%u\n", ( 0x000000ff & ip),
							((0x0000ff00 & ip) >> 8),
							((0x00ff0000 & ip) >> 16),
							((0xff000000 & ip) >> 24));
}

void debug_ipv4_address(uint32_t ip)
{
	printf("%u.%u.%u.%u\n", ((0xff000000 & ip) >> 24),
							((0x00ff0000 & ip) >> 16),
							((0x0000ff00 & ip) >> 8),
							( 0x000000ff & ip));
}

void    debug_mac_address(uint8_t *mac)
{
	if (mac)
		printf("%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx\n",
			mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void	debug_my_arp(t_arphdr arp)
{
	printf("- - - - - ARP - - - - - - -\n");
	printf("---- ----- bytes ----- ----\n");
	hexdump("ARP packet RAW data :", &arp, sizeof(t_arphdr));
	printf("---- ----- ----- ----- ----\n");
	printf("hw type				: %u\n", htons(arp.hw_type));
	printf("protocol type			: 0x%04x\n", htons(arp.protocol_type));
	printf("hw addr len			: %u\n", arp.hw_addr_len);
	printf("protocol addr len		: %u\n", arp.protocol_addr_len);
	printf("opcode				: %s\n", htons(arp.opcode) == 2 ? "REPLY" : htons(arp.opcode) == 1 ? "REQUEST" : "ERROR ARP OPCODE");
	printf("src MAC				: ");
	debug_mac_address(arp.src_mac);
	printf("src IP				: ");
	debug_network_address(arp.src_ip);
	printf("dest MAC			: ");
	debug_mac_address(arp.dst_mac);
	printf("dest IP				: ");
	debug_network_address(arp.dst_ip);
	printf("- - - - - - - - - - - -\n\n");
}

void	debug_arp(struct ether_arp arp)
{
	printf("- - - - - ARP - - - - - - -\n");
	printf("hw type				: %u\n", arp.arp_hrd);
	printf("protocol type		: %u\n", arp.arp_pro);
	printf("hw addr len			: %u\n", arp.arp_hln);
	printf("protocol addr len	: %u\n", arp.arp_pln);
	printf("opcode				: %d\n", arp.arp_op);
	printf("src MAC				: ");
	debug_mac_address(arp.arp_sha);
	printf("src IP				: ");
	debug_network_address((uint32_t)*arp.arp_spa);
	printf("dest MAC			: ");
	debug_mac_address(arp.arp_tha);
	printf("dest IP				: ");
	debug_network_address((uint32_t)*arp.arp_tpa);
	printf("- - - - - - - - - - - -\n\n");
}

void	debug_my_ethernet(t_ethernethdr eth)
{
	printf("- - - - - ETHERNET - - - - - - -\n");
	printf("---- ----- bytes ----- ----\n");
	hexdump("ETHERNET packet RAW data :", &eth, sizeof(t_ethernethdr));
	printf("---- ----- ----- ----- ----\n");
	printf("src MAC @			: ");
	debug_mac_address(eth.src_mac);
	printf("dst MAC @			: ");
	debug_mac_address(eth.dst_mac);
	printf("protocol			: %s\n", eth.protocol == htons(0x0806) ? "ARP" : "not ARP");
	printf("- - - - - - - - - - - -\n\n");
}

void	debug_ethernet(struct ether_header eth)
{
	printf("- - - - - ETHERNET - - - - - - -\n");
	printf("dest MAC @			: ");
	debug_mac_address(eth.ether_dhost);
	printf("src MAC @			: ");
	debug_mac_address(eth.ether_shost);
	printf("protocol			: %s\n", eth.ether_type == 0x0806 ? "ARP" : "not ARP");
	printf("- - - - - - - - - - - -\n\n");
}

void	debug_params(t_params params)
{
	printf("- - - - - params - - - - - - -\n");
	printf("src_mac		: ");
	debug_mac_address(params.src_mac);
	printf("src_ip		: ");
	debug_network_address(params.src_ip);
	printf("dest_mac	: ");
	debug_mac_address(params.dst_mac);
	printf("dest_ip		: ");
	debug_network_address(params.dst_ip);
	printf("opt		: %x\n", params.opt);
	printf("opt		: %s %s %s %s\n",
		(params.opt & FLOOD)			? "flood," : "", 
		(params.opt & PING)				? "ping," : "", 
		(params.opt & INTERFACE_NAME)	? "interface_name," : "", 
		(params.opt & VERBOSE)			? "verbose" : "");
	printf("- - - - - - - - - - - -\n\n");
}

void    debug_sockaddr_in(struct sockaddr_in *addr)
{
    printf("sockaddr_in :\n");
    printf("    sin_family      --> %s\n", addr->sin_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    sin_port        --> %hu\n", ntohs(addr->sin_port));
    printf("    s_addr          --> %u.%u.%u.%u\n", (addr->sin_addr.s_addr & 0x000000ff), ((addr->sin_addr.s_addr & 0x0000ff00) >> 8), ((addr->sin_addr.s_addr & 0x00ff0000) >> 16), ((addr->sin_addr.s_addr & 0xff000000) >> 24));
    printf("    sin_zero        --> %s\n", addr->sin_zero);
    printf("\n\n");
}

void    debug_sockaddr_ll(struct sockaddr_ll *addr)
{
    printf("sockaddr_ll :\n");
    printf("    sll_family		--> %s\n", addr->sll_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    sll_protocol	--> 0x%04hx\n", ntohs(addr->sll_protocol));
    printf("    sll_ifindex		--> %u\n", addr->sll_ifindex);
    printf("    sll_hatype		--> %hu\n", ntohs(addr->sll_hatype));
    printf("    sll_pkttype		--> %u\n", addr->sll_pkttype);
    printf("    sll_halen		--> %u\n", addr->sll_halen);
    printf("    MAC @		--> ");
	debug_mac_address(addr->sll_addr);
    printf("\n\n");
}

void    debug_addrinfo(struct addrinfo *addr)
{
    printf("addr infos :\n");
    printf("    ai_flags        --> %d\n", addr->ai_flags);
    printf("    ai_family       --> %s\n", addr->ai_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    ai_socktype     --> %s\n", addr->ai_socktype == SOCK_RAW ? "SOCK_RAW" : addr->ai_socktype == SOCK_DGRAM ? "SOCK_DGRAM" : "OTHER SOCK TYPE");
    printf("    ai_protocole    --> %d\n", addr->ai_protocol);
    printf("    ai_addrlen      --> %d\n", addr->ai_addrlen);
#ifdef __MAC__
    printf("    ai_addr len     --> %d\n", addr->ai_addr->sa_len);
#endif
    printf("    ai_addr fam     --> %d\n", addr->ai_addr->sa_family);
    printf("    ai_addr data    --> %s\n", addr->ai_addr->sa_data);
    printf("    ai_canonname    --> %s\n", addr->ai_canonname);
    printf("    ai_next         --> %p\n", addr->ai_next);
    printf("\n\n");
}

void hexdump(char *desc, void* addr, int len)
{
	int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    if (desc != NULL)
        printf ("%s:\n", desc);
    for (i = 0; i < len; i++) {
        if ((i % 16) == 0) {
            if (i != 0)
                printf("  %s\n", buff);
            printf("  %04x ", i);
        }
        i % 16 == 7 ? printf(" %02x -", pc[i]) : printf(" %02x", pc[i]);
        if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
            buff[i % 16] = '.';
        } else {
            buff[i % 16] = pc[i];
        }
        buff[(i % 16) + 1] = '\0';
    }
    while ((i % 16) != 0) {
        printf("   ");
        i++;
    }
    printf("  %s\n", buff);
}
