#include "ft_malcolm.h"

bool		check_args(int ac, char **av)
{
	t_options_check n_opt;

	ft_memset(&n_opt, 0, sizeof(t_options_check));
	for (int i = 4; i < ac; i++)
	{
		if (!ft_strcmp(av[i], "--flood") || !ft_strcmp(av[i], "-f"))
			n_opt.n_flood++;
		else if (!ft_strcmp(av[i], "--ping") || !ft_strcmp(av[i], "-p"))
			n_opt.n_ping++;
		else if ((!ft_strcmp(av[i], "--interface_name") || !ft_strcmp(av[i], "-i"))
			&& i + 1 < ac
			&& ft_strcmp(av[i + 1], "--flood")
			&& ft_strcmp(av[i + 1], "-f")
			&& ft_strcmp(av[i + 1], "--ping")
			&& ft_strcmp(av[i + 1], "-p")
			&& ft_strcmp(av[i + 1], "--verbose")
			&& ft_strcmp(av[i + 1], "-v"))
		{
			n_opt.n_interface_name++;
			++i;
		}
		else if (!ft_strcmp(av[i], "--verbose") || !ft_strcmp(av[i], "-v"))
			n_opt.n_verbose++;
		else
		{
			printf("ERROR : arg %s\n", av[i]);
			return true;
		}
	}
	if (n_opt.n_flood > 1 || n_opt.n_ping > 1 || n_opt.n_interface_name > 1 || n_opt.n_verbose > 1)
		return true;
	return false;
}

uint8_t		get_options(int ac, char **av)
{
	uint8_t	opt;

	opt = 0;
	(void)ac;
	(void)av;
	for (int i = 4; i < ac; i++)
	{
		if (!ft_strcmp(av[i], "--flood") || !ft_strcmp(av[i], "-f"))
			opt |= (1 * FLOOD);
		else if (!ft_strcmp(av[i], "--ping") || !ft_strcmp(av[i], "-p"))
			opt |= (1 * PING);
		else if (!ft_strcmp(av[i], "--interface_name") || !ft_strcmp(av[i], "-i"))
			opt |= (1 * INTERFACE_NAME);
		else if (!ft_strcmp(av[i], "--verbose") || !ft_strcmp(av[i], "-v"))
			opt |= (1 * VERBOSE);
	}
	return opt;
}

uint8_t		*get_mac_from_input(char *av)
{
	int		ret;
	uint8_t	*mac;
	char	**split;

	ret = OK;
	mac = NULL;
	split = NULL;
	mac = (uint8_t *)ft_memalloc(sizeof(uint8_t) * 6);
	if (!mac)
		return NULL;
	split = ft_strsplit(av, ':');
	if (!split)
	{
		ft_memdel((void **)&mac);
		return NULL;
	}
	if (listlen(split) != 6)
		return NULL;
	for (int i = 0; split[i]; i++)
	{
		if (ft_strlen(split[i]) != 2)
		{
			ret = KO;
			break ;
		}
		uint8_t tmp = 0;
		for (int j = 0; j < 2; j++)
		{
			if (!ft_isdigit(split[i][j])
				&& split[i][j] != 'a' && split[i][j] != 'b'
				&& split[i][j] != 'c' && split[i][j] != 'd'
				&& split[i][j] != 'e' && split[i][j] != 'f')
			{
				ret = KO;
				break ;
			}
			if (ft_isdigit(split[i][j]))
				tmp = (tmp << (4 * j)) + split[i][j] - 48;
			else
				tmp = (tmp << (4 * j)) + split[i][j] - 87;
		}
		mac[i] = tmp;
	}
	for (int i = 0; split[i]; i++)
		ft_memdel((void **)&split[i]);
	ft_memdel((void **)&split);
	if (ret == KO)
	{
		ft_memdel((void **)&mac);
	}
	return mac;
}

char		*get_interface_name(int ac, char **av)
{
	for (int i = 5; i < ac; i++)
	{
		if (!ft_strcmp(av[i - 1], "--interface_name") || !ft_strcmp(av[i - 1], "-i"))
			return ft_strdup(av[i]);
	}
	return NULL;
}

t_params	*set_default_options(void)
{
	t_params	*params;

	params = NULL;
	params = (t_params *)ft_memalloc(sizeof(t_params));
	if (!params)
		return NULL;
	params->interface_name = ft_strdup(DEFAULT_INTERFACE_NAME);
	params->opt |= (DEFAULT_FLOOD * FLOOD);
	params->opt |= (DEFAULT_PING * PING);
	params->opt |= (DEFAULT_VERBOSE * VERBOSE);
	return params;
}

t_params	*get_params_from_args(int ac, char **av)
{
	t_params	*params;

	(void)ac;
	(void)av;
	params = NULL;
	if (ac < 4)
		return NULL;
	params = set_default_options();
	params->opt |= get_options(ac, av);
	if (params->opt & INTERFACE_NAME)
	{
		ft_memdel((void **)&params->interface_name);
		params->interface_name = get_interface_name(ac, av);
		if (!params->interface_name)
		{
			ft_memdel((void **)&params);
			return NULL;
		}
	}

	params->src_name = ft_strdup(av[0]);
	if (!params->src_name)
	{
		ft_memdel((void **)&params->interface_name);
		ft_memdel((void **)&params);
		return NULL;
	}
	params->src_ip = get_ip_from_str(av[0]);
	if (!params->src_ip)
	{
		ft_memdel((void **)&params->src_name);
		ft_memdel((void **)&params->interface_name);
		ft_memdel((void **)&params);
		return NULL;
	}
	params->src_mac = get_mac_from_input(av[1]);
	if (!params->src_mac)
	{
		ft_memdel((void **)&params->src_name);
		ft_memdel((void **)&params->interface_name);
		ft_memdel((void **)&params);
		return NULL;
	}
	params->dst_ip = get_ip_from_str(av[2]);
	if (!params->dst_ip)
	{
		ft_memdel((void **)&params->src_name);
		ft_memdel((void **)&params->src_mac);
		ft_memdel((void **)&params->interface_name);
		ft_memdel((void **)&params);
		return NULL;
	}
	params->dst_mac = get_mac_from_input(av[3]);
	if (!params->dst_mac)
	{
		ft_memdel((void **)&params->src_name);
		ft_memdel((void **)&params->src_mac);
		ft_memdel((void **)&params->interface_name);
		ft_memdel((void **)&params);
		return NULL;
	}
	//debug_network_address(params->src_ip);
	//debug_network_address(params->dest_ip);

	return params;
}

t_params	*parsing(int ac, char **av)
{
	t_params	*params;

	params = NULL;
	if (ac < 4)
		return NULL;
	if (check_args(ac, av))
		return NULL;
	params = get_params_from_args(ac, av);
	return params;
}
