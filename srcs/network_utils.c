#include "ft_malcolm.h"

struct sockaddr_in      get_sockaddr_in_from_str(char *addr)
{
	struct sockaddr_in      target;

	ft_memset(&target, 0, sizeof(target));
	target.sin_family = AF_INET;
	//inet_pton(AF_INET, job->address, &(target.sin_addr));
	target.sin_addr.s_addr = get_ip_from_str(addr);
	return target;
}

struct sockaddr_ll      get_sockaddr_ll(char *interface_name, uint8_t *mac)
{
	uint32_t			interface_index;
	struct sockaddr_ll	target;

	ft_memset(&target, 0, sizeof(target));
	interface_index = if_nametoindex(interface_name);
	if (!interface_index)
	{
		printf("Error: fail to get interface index from interface name\n");
		return target;
	}
	target.sll_family = AF_PACKET;
	target.sll_protocol = htons(ETH_P_ARP);
	target.sll_ifindex = interface_index;
	target.sll_hatype = htons(ARPHRD_ETHER);
	target.sll_pkttype = 0;
	target.sll_halen = 6;
	ft_memcpy(target.sll_addr, mac, 6);
	return target;
}

struct addrinfo	*get_addrinfo_from_str(char *addr)
{
	struct addrinfo		*server;
	struct addrinfo		hints;

	ft_memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = PF_UNSPEC;
	if (getaddrinfo(addr, NULL, &hints, &server) != 0)
	{
		return NULL;
	}
	ft_memset(((struct sockaddr_in *)server->ai_addr)->sin_zero, 0, sizeof(((struct sockaddr_in *)server->ai_addr)->sin_zero));
	return server;
}

uint32_t	get_my_ip_addr(void)
{
	struct ifaddrs	*ifap;
	struct ifaddrs	*tmp;
	uint32_t		ret;

	ret = 0;
	if (getifaddrs(&ifap) == -1)
		return 0;
	tmp = ifap;
	while (ifap->ifa_next)
	{
		if (!ft_strncmp(ifap->ifa_name, "wlp4s0", 6) && ((struct sockaddr_in *)ifap->ifa_addr)->sin_family == AF_INET)
		{
			ret = ((struct sockaddr_in *)ifap->ifa_addr)->sin_addr.s_addr;
			ifap = tmp;
			freeifaddrs(ifap);
			return ret;
		}
		ifap = ifap->ifa_next;
	}
	ifap = tmp;
	freeifaddrs(ifap);
	return 0;
}

uint32_t	get_ip_from_str(char *addr)
{
	uint32_t		ipv4;
	struct addrinfo	*tmp;

	tmp = get_addrinfo_from_str(addr);
	if (!tmp)
		return 0;
	ipv4 = ((struct sockaddr_in *)tmp->ai_addr)->sin_addr.s_addr;
	freeaddrinfo(tmp);
	return ipv4;
}

char	*getlocalhost(void)
{
	struct ifaddrs	*id;
	struct ifaddrs	*ifa;
	char			*interface_name;

	interface_name = NULL;
	if (getifaddrs(&id) == -1)
		return NULL;
	for (ifa = id; ifa != NULL; ifa = ifa->ifa_next)
	{
		if (ifa->ifa_addr != NULL && ifa->ifa_addr->sa_family == AF_INET)
		{
			interface_name = ft_strdup(ifa->ifa_name);
			break;
		}
	}
	if (ifa == NULL)
	{
		printf("ERROR: network interface not found\n");
		return NULL;
	}
	freeifaddrs(id);
	return interface_name;
}

uint8_t		*get_my_mac_address(char *interface_name)
{
	uint8_t			*my_mac;
	char			*line;
	int				fd;
	char			filename[23 + ft_strlen(interface_name) + 1];

	(void)fd;
	(void)line;
	my_mac = NULL;
	ft_memset(filename, 0, 23 + ft_strlen(interface_name) + 1);
	ft_memcpy(filename, "/sys/class/net/////////", 24);
	ft_memcpy(filename + 15, interface_name, ft_strlen(interface_name));
	ft_memcpy(filename + 15 + ft_strlen(interface_name) + 1, "address", ft_strlen("address"));
	fd = open(filename, O_RDONLY);
	if (fd == -1)
	{
		printf("Error: fail to get interface name\n");
		return NULL;
	}
	if (get_next_line(fd, &line) == -1)
	{
		ft_memdel((void **)&line);
		close(fd);
		return NULL;
	}
	line[ft_strlen(line) - 1] = 0;
	my_mac = get_mac_from_input(line);
	ft_memdel((void **)&line);
	close(fd);
	return my_mac;
}

uint8_t		*hton_mac(uint8_t *mac)
{
	uint8_t		tmp;
	uint8_t		*inverted;

	inverted = (uint8_t *)ft_memalloc(sizeof(uint8_t) * 6);
	if (!inverted)
		return NULL;
	if (!ft_memcpy(inverted, mac, sizeof(uint8_t) * 6))
	{
		ft_memdel((void **)&inverted);
		return NULL;
	}
	for (int i = 0; i < 3; i++)
	{
		tmp = inverted[i];
		inverted[i] = inverted[6 - 1 - i];
		inverted[6 - 1 - i] = tmp;
	}
	return inverted;
}
