#include "utils.h"

char		*ft_strdup(char const *str)
{
	int		i;
	char	*dest;

	i = 0;
	dest = NULL;
	if (str)
	{
		if (!(dest = (char *)malloc(sizeof(char) * ft_strlen(str) + 1)))
			return (NULL);
		while (str[i])
		{
			dest[i] = str[i];
			++i;
		}
		dest[i] = '\0';
	}
	return (dest);
}
