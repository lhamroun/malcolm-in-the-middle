#include "utils.h"

char	*ft_strcat(char *dest, char const *src)
{
	int		i;
	int		len_d;

	i = 0;
	len_d = ft_strlen(dest);
	if (src == NULL)
		return (dest);
	while (src[i])
	{
		dest[i + len_d] = src[i];
		++i;
	}
	dest[i + len_d] = '\0';
	return (dest);
}
