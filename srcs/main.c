#include "ft_malcolm.h"

int		main(int ac, char **av)
{
	int			ret;
	t_params	*params;

	ret = 0;
	if (getuid())
	{
		printf("You need to be root to run ./ft_malcolm\n");
		printf("usage: sudo ./ft_malcolm <src IP> <src MAC @> <dest IP> <dest MAC @> \n\
			[--flood | -f]          : flood ARP reply to the target\n\
			[--ping | -p]           : ping target before attack\n\
			[--interface_name | -i] : change network interface\n\
			[--verbose | -v]        : print received packets and sent ARP\n");
		return EXIT_SUCCESS;
	}
	params = parsing(ac - 1, av + 1);
	if (!params)
	{
		printf("usage: ./ft_malcolm <src IP> <src MAC @> <dest IP> <dest MAC @> \n\
			[--flood | -f]          : flood ARP reply to the target\n\
			[--ping | -p]           : ping target before attack\n\
			[--interface_name | -i] : change network interface\n\
			[--verbose | -v]        : print received packets and sent ARP\n");
		return EXIT_FAILURE;
	}
	ret = ft_malcolm(params);
	ft_memdel((void **)&params->src_name);
	ft_memdel((void **)&params->src_mac);
	ft_memdel((void **)&params->dst_mac);
	ft_memdel((void **)&params->interface_name);
	ft_memdel((void **)&params);
	return (ret);
}
