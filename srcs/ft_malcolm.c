#include "ft_malcolm.h"

void	*g_ptr;

void	intQuitHandler(int sig)
{
	t_params	*params;

	(void)sig;
	printf("\nintQuitHandler : Exit with Ctrl+C\n");
	params = g_ptr;
	params->opt = params->opt & ~FLOOD;
}

void	sigQuitHandler(int sig)
{
	(void)sig;
	printf("\nsigQuitHandler : Exit with Ctrl+C\n");
	g_ptr = NULL;
}

int			get_ARP_socket(void)
{
	int				fd;
	int				ret;
	struct timeval	time = {.tv_sec = 0, .tv_usec = 100000};

	fd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
	ret = setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &time, sizeof(struct timeval));
	if (ret == -1)
	{
		close(fd);
		return -1;
	}
	return fd;
}

uint8_t		ft_malcolm(t_params *params)
{
	uint8_t		ret;
	int			arp_sock_fd;
	uint8_t		*tmp;

	ret = 0;
	arp_sock_fd = -1;
	g_ptr = (void *)params;
	arp_sock_fd = get_ARP_socket();
	if (arp_sock_fd == -1)
		return KO;
	//printf("arp socket fd : %d\n", arp_sock_fd);
	signal(SIGINT, sigQuitHandler);
	tmp = get_my_mac_address(params->interface_name);
	if (!tmp)
	{
		printf("Error: interface %s not found\n", params->interface_name);
		ft_memdel((void **)&tmp);
		close(arp_sock_fd);
		return KO;
	}
	printf("Found available interface: %s\n", params->interface_name);
	ret = listen_ARP_request_from_target(arp_sock_fd, params);

	if (ret == KO || !g_ptr)
	{
		ft_memdel((void **)&tmp);
		close(arp_sock_fd);
		return KO;
	}
	signal(SIGINT, intQuitHandler);
	ret = reply_to_ARP_request(arp_sock_fd, params);
	ft_memdel((void **)&tmp);
	close(arp_sock_fd);
	printf("Exiting program...\n");
	return ret;
}
