#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

# define BUFF_SIZE 4096

void		ft_memdel(void **s);
void		*ft_memset(void *s, int c, size_t n);
void		*ft_memalloc(size_t size);
void		*ft_memcpy(void *dest, void const *src, size_t n);
int			ft_atoi(char const *str);
int			ft_strcmp(char const *s1, char const *s2);
int			str_is_number(char *str);
int			ft_isalpha(int c);
int			ft_isblank(char c);
int			ft_isdigit(int c);
int			listlen(char **tab);
int			is_str_in_tab(char const *str, char **tab);
char		**ft_strsplit(char const *s, char c);
char		*ft_strsub(char const *s, unsigned int start, size_t len);
char		*ft_strstr(char const *s1, char const *s2);
size_t		ft_strlen(char const *s);
char		*ft_strdup(char const *str);
int			get_next_line(int const fd, char **line);
char		*ft_strjoin(char const *s1, char const *s2);
char		*ft_strnew(size_t size);
char		*ft_strchr(char const *s, int c);
void		ft_strdel(char **as);
void		*ft_memchr(void const *s, int c, size_t n);
char		*ft_strcpy(char *dest, char const *src);
char		*ft_strcat(char *dest, char const *src);
int			ft_strncmp(char const *s1, char const *s2, size_t n);
int			ft_memcmp(void const *dest, void const *src, size_t n);
int			ft_strcmp(char const *s1, char const *s2);

#endif
