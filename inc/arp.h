#ifndef ARP_H
#define ARP_H

#include <stdlib.h>

#ifdef __MAC__

// from linux socket.h
#define AF_PACKET 17

#endif

// from if_ether.h
#define ETH_P_ARP 0x0806

typedef struct	s_arphdr
{
	uint16_t		hw_type;
	uint16_t		protocol_type;
	uint8_t			hw_addr_len;
	uint8_t			protocol_addr_len;
	uint16_t		opcode;
	uint8_t			src_mac[6];
	uint32_t		src_ip;
	uint8_t			dst_mac[6];
	uint32_t		dst_ip;
}					__attribute__((packed)) t_arphdr;

typedef struct	s_ethernethdr
{
	uint8_t			src_mac[6];
	uint8_t			dst_mac[6];
	uint16_t		protocol;
}					__attribute__((packed)) t_ethernethdr;

#endif
