#ifndef FT_MALCOLM_H
#define FT_MALCOLM_H

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <limits.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/stat.h>

#include <poll.h>
#include <ifaddrs.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netinet/ip_icmp.h>
#include <netinet/in.h>
#ifndef __MAC__
#include <netpacket/packet.h>
#endif

#include <net/if.h>
#include <net/if_arp.h>
#include <net/ethernet.h>
#include <netinet/if_ether.h>


#include "utils.h"
#include "arp.h"
#include "config.h"

#ifdef __MAC__
#define ETH_P_IP 0x0800

struct sockaddr_ll {
    unsigned short sll_family;   // Always AF_PACKET
    unsigned short sll_protocol; // Ethernet protocol type (e.g., ETH_P_IP for IP)
    int            sll_ifindex;  // Interface index
    unsigned short sll_hatype;   // Hardware type (e.g., ARPHRD_ETHER for Ethernet)
    unsigned char  sll_pkttype;  // Packet type (e.g., PACKET_HOST for incoming packets)
    unsigned char  sll_halen;    // Length of hardware address (MAC address)
    unsigned char  sll_addr[8];  // Hardware address (MAC address)
};
#endif

typedef struct	s_params
{
	// src is target @
	char		*src_name;
	uint8_t		*src_mac;
	uint32_t	src_ip;
	// dest is router @
	uint8_t			*dst_mac;
	uint32_t		dst_ip;
	uint8_t			opt;
	char			*interface_name;
}				t_params;

typedef struct		s_options_check
{
	uint8_t			n_flood;
	uint8_t			n_ping;
	uint8_t			n_interface_name;
	uint8_t			n_verbose;
}					t_options_check;

uint8_t		ft_malcolm(t_params *params);
t_params	*parsing(int ac, char **av);
uint8_t		*get_mac_from_input(char *av);

uint32_t			get_ip_from_str(char *addr);
uint32_t			get_my_ip_addr(void);
struct addrinfo		*get_addrinfo_from_str(char *addr);
char				*getlocalhost(void);
uint8_t				*hton_mac(uint8_t *mac);
uint8_t				*get_my_mac_address(char *interface_name);
struct sockaddr_in	get_sockaddr_from_str(char *addr);
struct sockaddr_ll	get_sockaddr_ll(char *interface_name, uint8_t *mac);

uint8_t		reply_to_ARP_request(int fd, t_params *params);

uint8_t		listen_ARP_request_from_target(int fd, t_params *params);

void hexdump(char *desc, void* addr, int len);
void		debug_addrinfo(struct addrinfo *addr);
void		debug_sockaddr_in(struct sockaddr_in *addr);
void		debug_sockaddr_ll(struct sockaddr_ll *addr);
void		debug_params(t_params params);
void		debug_mac_address(unsigned char *mac);
void		debug_ipv4_address(uint32_t ip);
void		debug_network_address(uint32_t ip);
void		debug_my_arp(t_arphdr arp);
void		debug_arp(struct ether_arp arp);
void		debug_my_ethernet(t_ethernethdr eth);
void		debug_ethernet(struct ether_header eth);


#endif
