#ifndef CONFIG_H
#define CONFIG_H

#define OK 0
#define KO 1

#define FLOOD 1
#define PING 2
#define VERBOSE 4
#define INTERFACE_NAME 8

#define DEFAULT_FLOOD 0
#define DEFAULT_PING 0
#define DEFAULT_VERBOSE 0
#define DEFAULT_INTERFACE_NAME "wlp4s0"

#endif
