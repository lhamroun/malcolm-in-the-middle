NAME = ft_malcolm

FLAG = -Wall -Wextra -Werror

OS_NAME := $(shell uname)
ifeq ($(OS_NAME),Darwin)
	FLAG += -D __MAC__
endif

HEADER = inc/ft_malcolm.h inc/utils.h inc/arp.h inc/config.h
INCLUDES = -I inc/

SRCS_PATH = srcs/
SRCS_NAME = main.c parsing.c ft_malcolm.c debug.c network_utils.c reply.c listener.c

UTILS_PATH = utils/
UTILS_NAME = ft_memalloc.c ft_memset.c ft_memcpy.c ft_memdel.c ft_strsplit.c \
			ft_strsub.c listlen.c ft_isdigit.c ft_strncmp.c ft_strlen.c ft_strdup.c \
			ft_memcmp.c ft_strcmp.c ft_strnew.c ft_strchr.c ft_strdel.c ft_strjoin.c \
			ft_memchr.c ft_strcat.c ft_strcpy.c get_next_line.c
UTILS = $(addprefix $(UTILS_PATH), $(UTILS_NAME))
SRCS_NAME += $(UTILS)

SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(FLAG) $(INCLUDES) -o $(NAME) $(OBJS)

$(OBJS_PATH)%.o:$(SRCS_PATH)%.c $(HEADER)
	mkdir -p $(OBJS_PATH)
	mkdir -p $(addprefix $(OBJS_PATH), $(UTILS_PATH))
	$(CC) $(FLAG) $(INCLUDES) -o $@ -c $<

clean:
	$(RM) -rf $(OBJS_PATH)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
