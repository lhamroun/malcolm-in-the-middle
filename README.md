# Malcolm in the middle

This project is about ARP spoofing / ARP poisoning, an introduction to Man in the Middle attacks.
This attack is possible using a vulnerability present in the the way the ARP protocol works and interacts in a network.

Project specifications are here : https://cdn.intra.42.fr/pdf/pdf/88997/en.subject.pdf
